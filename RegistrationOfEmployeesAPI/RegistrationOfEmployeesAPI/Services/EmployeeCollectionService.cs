﻿using MongoDB.Driver;
using RegistrationOfEmployeesAPI.Models;
using RegistrationOfEmployeesAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationOfEmployeesAPI.Services
{
    public class EmployeeCollectionService : IEmployeeCollectionService
    {
        private readonly IMongoCollection<Employee> _employee;

        public EmployeeCollectionService(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _employee = database.GetCollection<Employee>(settings.NoteCollectionName);
        }


         public async Task<bool> Create(Employee employee)
          {
            await _employee.InsertOneAsync(employee);
            return true;
          }


        public async Task<bool> Delete(Guid id)
        {
            var result = await _employee.DeleteOneAsync(employee => employee.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;

        }

        public async Task<List<Employee>> GetAllEmployee()
        {
            var result = await _employee.FindAsync(employee => true);
            return result.ToList();
        }

         public async Task<bool> Update(Guid id, Employee employees)
          {
            employees.Id = id;
            var result = await _employee.ReplaceOneAsync(employees => employees.Id == id, employees);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _employee.InsertOneAsync(employees);
                return false;
            }
            return true;
        }
    }
    }
