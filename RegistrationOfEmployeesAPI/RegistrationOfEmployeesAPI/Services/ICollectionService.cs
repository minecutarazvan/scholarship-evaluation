﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationOfEmployeesAPI.Services
{
    public interface ICollectionService<T>
    {
        Task<List<T>> GetAllEmployee();

        Task<bool> Create(T employee);

        Task<bool> Update(Guid id, T employee);

        Task<bool> Delete(Guid id);

    }
}
