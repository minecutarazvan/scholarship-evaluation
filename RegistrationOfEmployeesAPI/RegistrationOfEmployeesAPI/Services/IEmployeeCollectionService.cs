﻿using RegistrationOfEmployeesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationOfEmployeesAPI.Services
{
  public interface IEmployeeCollectionService: ICollectionService<Employee>
    { 

    }
}
