﻿using Microsoft.AspNetCore.Mvc;
using RegistrationOfEmployeesAPI.Models;
using RegistrationOfEmployeesAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationOfEmployeesAPI.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {

        IEmployeeCollectionService _employeeCollectionService;
        public EmployeeController(IEmployeeCollectionService employeeCollectionService)
        {
            _employeeCollectionService = employeeCollectionService;
        }
        
         /// <summary>
         /// Return all employees
         /// </summary>
         /// <returns></returns>
         [HttpGet]
         public async Task<IActionResult> GetEmployees()
         {
           List<Employee> employee = await _employeeCollectionService.GetAllEmployee();
            return Ok(employee);
         }
        
         /// <summary>
         /// Creat a new employee
         /// </summary>
         /// <param name=""></param>
         /// <returns></returns>
         [HttpPost]
         public async Task<IActionResult> CreateEmployees([FromBody] Employee employee)
         {
             if (employee == null)
             {
                 return BadRequest("Employee list is empty");
             }
            if ( await _employeeCollectionService.Create(employee))
            {
                return CreatedAtRoute( new { id = employee.Id.ToString() }, employee);
            }
            return NoContent();
         }

         /// <summary>
         /// Update Employee
         /// </summary>
         /// <response code="400">Bad Request</response>
         /// <response code="204">NotFound</response>
         /// <returns></returns>
         [HttpPut("{id}")]
         public async Task<IActionResult> UpdateEmployee([FromBody]Employee updateEmployee,Guid id)
         {
             if(updateEmployee == null)
             {
                 return BadRequest("Employee list is empty");
             }
             bool index = await _employeeCollectionService.Update(id,updateEmployee);
             if(index == false)
             {
                 return NotFound();
             }
             
             return Ok(updateEmployee);
         }
        
         /// <summary>
         /// Delete employee
         /// </summary>
         /// <param name="id"></param>
         /// <returns></returns>
         [HttpDelete("{id}")]
         public async Task<IActionResult> DeleteEmployee(Guid id)
         {
            bool index =await _employeeCollectionService.Delete(id);
             if (index == true)
             {
                 return NotFound();
             }
             return NoContent();
         }
     }
    }

