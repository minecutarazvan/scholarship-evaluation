import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../Model/employee';
import { ServicesService } from '../Services/services.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  employees: Employee[] =[];
  constructor(private employeeService:ServicesService,private router:Router) { }

  ngOnInit(): void {
   this.getEmployee();
  };

  getEmployee(){
    this.employeeService.getEmployee().subscribe((result)=> {
      this.employees = result;
    }) 
  }
  deleteNote(id: string){
    this.employeeService.deleteNote(id).subscribe(() => this.getEmployee());
  }
}
