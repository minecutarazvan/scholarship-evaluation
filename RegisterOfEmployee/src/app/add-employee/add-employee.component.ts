import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesService } from '../Services/services.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

  name: string = "";
  surName: string = "";
  
  constructor(private _activatedRoute: ActivatedRoute,private employeeService:ServicesService,private router: Router) { }

  ngOnInit(): void {
    this._activatedRoute.queryParams.subscribe(params =>{
      this.name=params['employeename'];
      this.surName=params['employeesurName'];
    });
 }

  add(){
    this.employeeService.addEmployee(this.name,this.surName).subscribe(() => this.router.navigateByUrl(''));
       }
}
