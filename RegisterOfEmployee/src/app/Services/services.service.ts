import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employee } from '../Model/employee';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  readonly baseUrl= "http://localhost:5000";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };
  constructor(private router : Router, private httpClient: HttpClient) { }

  getEmployee(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.baseUrl+"/employee",this.httpOptions);
  }

  addEmployee(employeeName: string,employeesurName: string): Observable<Employee>{
    const employee: Employee={
      name: employeeName,
      surName: employeesurName,
    };
   return this.httpClient.post<Employee>(this.baseUrl+"/employee",employee); 
 }
 deleteNote(id: string){
  return this.httpClient.delete(this.baseUrl+ '/employee/'+id);
}

}
